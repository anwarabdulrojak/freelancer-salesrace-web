<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

date_default_timezone_set("Asia/Jakarta"); 

function Pecah_tanggal($date){
    $data['hari'] = substr($date, 0, 2);
    $data['bulan'] = substr($date, 3, 2);
    $data['tahun'] = substr($date, 6, 4);
    
    return $data;
} 

function Pecah_tanggal2($date){
    $data['hari'] = substr($date, 8, 2);
    $data['bulan'] = substr($date, 5, 2);
    $data['tahun'] = substr($date, 0, 4);
    
    if(substr($date, 11, 8)){
        $data['waktu'] = substr($date, 11, 8);
    } else {
        $data['waktu'] = "";
    }
    
    return $data;
} 

function Conv_tanggal($date){
    $data = Pecah_tanggal($date);    
    return $data['tahun']."-".$data['bulan']."-".$data['hari'];
} 

function Univ_tanggal($date){
    $data = Pecah_tanggal2($date);
    if($data['waktu']){
        return $data['hari']."-".$data['bulan']."-".$data['tahun']." ".$data['waktu'];
    } else {
        return $data['hari']."-".$data['bulan']."-".$data['tahun'];
    }
}

function get_mpp_sales(){    
    $CI =& get_instance();
    $CI->load->model('UserModel');
    $jumlah_mpp_sales = $CI->UserModel->get_all_sales();
    $jumlah_mpp_sales = count($jumlah_mpp_sales);

    return $jumlah_mpp_sales;
}

function get_poin_sales(){
    $CI =& get_instance();
    $month = date("m");
    $year = date("Y");

    $week = date("W", strtotime($year . "-" . $month ."-01")); // weeknumber of first day of month
    $date[] = date("d-m-Y", strtotime($year . "-" . $month ."-01")); // first day of month
    $unix = strtotime($year."W".$week ."+1 week");
    while(date("m", $unix) == $month){ // keep looping/output of while it's correct month

        $date[] = date("d-m-Y", $unix-86400); // Sunday of previous week
        $date[] = date("d-m-Y", $unix); // this week's monday
        $unix = $unix + (86400*7);
    }
    $date[] = date("d-m-Y", strtotime("last day of ".$year . "-" . $month)); //echo last day of month

    $i = 1;
    $poin_sales = 0;
    $pencapaian_mingguan_sales_array = array();
    foreach($date as $key => $value){
        $target_sales = 0;
        $pencapaian_sales = 0;
        $pencapaian_sales_persen = 0;
        if($key%2!=0){
            continue;
        }
        $start_date = date("Y-m-d", strtotime($value));
        $end_date = date("Y-m-d", strtotime($date[$key+1]));

        $CI->load->model('TargetModel');
        $where_target_sales = array('week'=> $i, 'start_date'=> $start_date,'end_date'=> $end_date);
        $target_sales = $CI->TargetModel->select($where_target_sales);
        $target_sales = (!empty($target_sales)) ? $target_sales->nilai : 0;
        
        $CI->load->model('AchievementModel');
        $pencapaian_sales = $CI->AchievementModel->pencapaian_nilai($start_date,$end_date);
        $pencapaian_sales = (!empty($pencapaian_sales)) ? $pencapaian_sales : 0;
        $pencapaian_sales_persen = ($pencapaian_sales > 0) ? ($pencapaian_sales / $target_sales) * 100 : 0;
        $pencapaian_mingguan_sales_array[] = $pencapaian_sales;
            
        if($pencapaian_sales_persen < 95){
            $poin_sales += 0;
        }else if ($pencapaian_sales_persen >= 95 && $pencapaian_sales_persen < 100) {
            $poin_sales += 1;
        }else if ($pencapaian_sales_persen >= 100 && $pencapaian_sales_persen < 105) {
            $poin_sales += 2;
        }else if ($pencapaian_sales_persen >= 105){
            $poin_sales += 5;
        }
        $i++;
    }

    $pencapaian_mingguan_sales_sum = array_sum($pencapaian_mingguan_sales_array);
    $pencapaian_mingguan_sales_count = count($pencapaian_mingguan_sales_array);
    $maximal_persen_sales_month = 100 * $pencapaian_mingguan_sales_count;

    $pencapaian_sales_bulan = ($pencapaian_mingguan_sales_sum / $maximal_persen_sales_month) * 100;
    if($pencapaian_sales_bulan < 95){
        $poin_sales += 0;
    }else if ($pencapaian_sales_bulan >= 95 && $pencapaian_sales_bulan < 100) {
        $poin_sales += 1;
    }else if ($pencapaian_sales_bulan >= 100 && $pencapaian_sales_bulan < 105) {
        $poin_sales += 2;
    }else if ($pencapaian_sales_bulan >= 105){
        $poin_sales += 3;
    }

    return $poin_sales;
}

function get_poin_individu($user_id, $kategori_id){
    $CI =& get_instance();
    $month = date("m");
    $year = date("Y");

    $week = date("W", strtotime($year . "-" . $month ."-01")); // weeknumber of first day of month
    $date[] = date("d-m-Y", strtotime($year . "-" . $month ."-01")); // first day of month
    $unix = strtotime($year."W".$week ."+1 week");
    while(date("m", $unix) == $month){ // keep looping/output of while it's correct month

        $date[] = date("d-m-Y", $unix-86400); // Sunday of previous week
        $date[] = date("d-m-Y", $unix); // this week's monday
        $unix = $unix + (86400*7);
    }
    $date[] = date("d-m-Y", strtotime("last day of ".$year . "-" . $month)); //echo last day of month

    $i = 1;
    $poin_individu = 0;
    $pencapaian_mingguan_individu_array = array();
    foreach($date as $key => $value){
        $target_individu = 0;
        $pencapaian_individu = 0;
        $pencapaian_individu_persen = 0;
        if($key%2!=0){
            continue;
        }
        $start_date = date("Y-m-d", strtotime($value));
        $end_date = date("Y-m-d", strtotime($date[$key+1]));

        $CI->load->model('TargetModel');
        $where_target_individu = array('kategori_id'=> $kategori_id, 'week'=> $i, 'start_date'=> $start_date,'end_date'=> $end_date);
        $target_individu = $CI->TargetModel->select($where_target_individu);
        $target_individu = (!empty($target_individu)) ? $target_individu->nilai : 0;
        
        $CI->load->model('AchievementModel');
        $pencapaian_individu = $CI->AchievementModel->pencapaian_nilai_user($user_id, $start_date, $end_date);
        $pencapaian_individu = (!empty($pencapaian_individu)) ? $pencapaian_individu : 0;
        $pencapaian_individu_persen = ($pencapaian_individu > 0) ? ($pencapaian_individu / $target_individu) * 100 : 0;
        $pencapaian_mingguan_individu_array[] = $pencapaian_individu;
            
        if($pencapaian_individu_persen < 95){
            $poin_individu += 0;
        }else if ($pencapaian_individu_persen >= 95 && $pencapaian_individu_persen < 100) {
            $poin_individu += 1;
        }else if ($pencapaian_individu_persen >= 100 && $pencapaian_individu_persen < 105) {
            $poin_individu += 2;
        }else if ($pencapaian_individu_persen >= 105){
            $poin_individu += 5;
        }
        $i++;
    }

    $pencapaian_mingguan_individu_sum = array_sum($pencapaian_mingguan_individu_array);
    $pencapaian_mingguan_individu_count = count($pencapaian_mingguan_individu_array);
    $maximal_persen_individu_month = 100 * $pencapaian_mingguan_individu_count;

    $pencapaian_individu_bulan = ($pencapaian_mingguan_individu_sum / $maximal_persen_individu_month) * 100;
    if($pencapaian_individu_bulan < 95){
        $poin_individu += 0;
    }else if ($pencapaian_individu_bulan >= 95 && $pencapaian_individu_bulan < 100) {
        $poin_individu += 1;
    }else if ($pencapaian_individu_bulan >= 100 && $pencapaian_individu_bulan < 105) {
        $poin_individu += 2;
    }else if ($pencapaian_individu_bulan >= 105){
        $poin_individu += 3;
    }

    return $poin_individu;

}

function get_data_store(){
    $CI =& get_instance();

    $month = date("m");
    $year = date("Y");
    $CI->load->model('TargetStoreModel');
    $target_store = $CI->TargetStoreModel->select(array('month'=>$month,'year'=>$year));
    $target_store =  (!empty($target_store)) ? $target_store->nilai : 0;

    $CI->load->model('AchievementStoreModel');
    $pencapaian_store =  $CI->AchievementStoreModel->select(array("date BETWEEN '$month' AND '$year'"));
    $pencapaian_store = (!empty($pencapaian_store)) ? $pencapaian_store->nilai : 0;

    $pencapaian_store_persen = ($pencapaian_store > 0) ? ($pencapaian_store / $target_store) * 100 : 0;
    
    $nilai_insentif_persen = 0;
    if($pencapaian_store_persen >= 95 && $pencapaian_store_persen < 100){
        $nilai_insentif_persen = 0.4;
    }else if ($pencapaian_store_persen >= 100 && $pencapaian_store_persen < 120) {
        $nilai_insentif_persen = 0.8;
    }else if ($pencapaian_store_persen >= 120){      
        $nilai_insentif_persen = 1;
    }
    $nilai_insentif = $nilai_insentif_persen * $pencapaian_store;

    $data['target_store'] = $target_store;
    $data['pencapaian_store'] = $pencapaian_store;
    $data['pencapaian_store_persen'] = $pencapaian_store_persen;
    $data['nilai_insentif_persen'] = $nilai_insentif_persen;
    $data['nilai_insentif'] = $nilai_insentif;

    return $data;
}

function get_persen_insentif_per_level(){

    $bobot_poin_sales = 1.5;
    $jumlah_mpp_sales = get_mpp_sales();
    $poin_sales = get_poin_sales();

    $poin_per_level = $bobot_poin_sales * $jumlah_mpp_sales;
    $persen_insentif_level = ($poin_sales > 0) ? (($poin_per_level / $poin_sales) * 100) : 0;

    return $persen_insentif_level;
}

if (!function_exists('formatTanggal')) {
	function formatTanggal($timestamp = '', $date_format = 'd F Y', $suffix = '') {
		// $date_format = 'l, j F Y | H:i';
		if (trim ($timestamp) == '')
		{
				$timestamp = time ();
		}
		elseif (!ctype_digit ($timestamp))
		{
			$timestamp = strtotime ($timestamp);
		}
		# remove S (st,nd,rd,th) there are no such things in indonesia :p
		$date_format = preg_replace ("/S/", "", $date_format);
		$pattern = array (
			'/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
			'/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
			'/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
			'/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
			'/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
			'/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
			'/April/','/June/','/July/','/August/','/September/','/October/',
			'/November/','/December/',
		);        
		$replace = array ( 'Mon','Tue','Wed','Thu','Fri','Sat','Sun',
			'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
			'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Aug','Sep','Okt','Nov','Des',
			'Januari','Februari','Maret','April','Juni','Juli','Agustus','September',
			'Oktober','November','Desember',
		);
		$date = date ($date_format, $timestamp);
		$date = preg_replace ($pattern, $replace, $date);
		$date = "{$date} {$suffix}";
		return trim($date);
	}
}

if (!function_exists('formatNumber')) {
	/* Undocumented function
	 *
	 * @param integer $value
	 * @param string $format
	 * @param boolean $symbol
	 * @return string
	 */
	function formatNumber($angka){

		$hasil_rupiah = number_format($angka,0,',','.');
		return $hasil_rupiah;
	
	}
}

//Dashboard
function getSTD(){

    $total_hari = date('t');
	$tanggal = date('d');
	
	$hitung = $tanggal / $total_hari;
	$persentase = $hitung * 100;
	$pembulatan = round($persentase,2);

	return $pembulatan;
}

function getTargetStore(){
    $CI =& get_instance();

    $month = date("m");
    $year = date("Y");
    $CI->load->model('TargetModel');
    $target_store = $CI->TargetModel->select(array('month'=>$month, 'year'=> $year, 'type'=> 1));
    $target_store =  (!empty($target_store)) ? $target_store->nilai : 0;

    return $target_store;
}

function getSalesAch(){
    $CI =& get_instance();

    $month = date("m");
    $year = date("Y");
    $CI->load->model('AchievementModel');
	$pencapaian =  $CI->AchievementModel->achievement_sales($month, $year);

    return $pencapaian;
}

function getPercentage($ach, $target){
	$hitung = $ach * 100;
	$hitung = ($hitung > 0) ? $hitung : 0;
	if($target > 0){
		$pembagian = $hitung / $target;
		$pembulatan = ($pembagian > 0) ? round($pembagian,2) : '0';
		return $pembulatan;
	}
	return $hitung;
}

function getTargetDept($department_id){
    $CI =& get_instance();

    $month = date("m");
    $year = date("Y");
    $CI->load->model('TargetModel');
    $target = $CI->TargetModel->select(array('month' => $month,'year' => $year,'department_id' => $department_id,'type' => 2));
    $target =  (!empty($target)) ? $target->nilai : 0;

    return $target;
}

function getAchSalesDept($department_id){
    $CI =& get_instance();

    $CI->load->model('AchievementModel');
	$pencapaian =  $CI->AchievementModel->getAchSalesByDept($department_id);

	$achievement = 0;
	foreach($pencapaian as $value){
		$achievement += $value->achievement_nilai;
	}

    return $achievement;
}

function getGAPToSTD($department_id){
	$std = getSTD();
	$persentase = getPercentage(getAchSalesDept($department_id), getTargetDept($department_id));

	$gapt_to_std = $persentase - $std;
    return $gapt_to_std;
}

function getMPPDepartment($department_id){
    $CI =& get_instance();

    $CI->load->model('UserModel');
    $jumlah = $CI->UserModel->num_rows(array('department_id' => $department_id,'role' => 2));

    return $jumlah;
}

function getProdMPP($department_id){
    $achievement = getSalesAch($department_id);
	$jumlah_mpp = getMPPDepartment($department_id);
	
	$hitung = 0;
	if ($achievement > 0) {
		$hitung = $achievement / $jumlah_mpp;
	}
    return $hitung;
}

function getProdArea($department_id, $luasan_area){
	$achievement = getSalesAch($department_id);
	
	$hitung = 0;
	if ($achievement > 0) {
		$hitung = $achievement / $luasan_area;
	}

    return $hitung;
}

function getDept($department_id){
    $CI =& get_instance();

    $month = date("m");
    $year = date("Y");
    $CI->load->model('DepartmentModel');
    $dept = $CI->DepartmentModel->select(array('id' => $department_id));
    $dept =  (!empty($dept)) ? $dept->nama : '';

    return $dept;
}

function getTragetSales($sales_id){
    $CI =& get_instance();

    $month = date("m");
    $year = date("Y");
    $CI->load->model('TargetModel');
    $target = $CI->TargetModel->select(array('month' => $month,'year' => $year,'sales_id' => $sales_id,'type' => 3));
    $target =  (!empty($target)) ? $target->nilai : 0;

    return $target;
}

function getAchSales($sales_id){
    $CI =& get_instance();

    $CI->load->model('AchievementModel');
	$pencapaian =  $CI->AchievementModel->getAchSales($sales_id);
	$achievement =  (!empty($pencapaian)) ? $pencapaian->achievement_nilai : 0;

    return $achievement;
}

function getMTDAch($sales_id,$date){
    $CI =& get_instance();

    $CI->load->model('AchievementModel');
	$pencapaian =  $CI->AchievementModel->getMTDAch($sales_id,$date);
	$achievement =  (!empty($pencapaian)) ? $pencapaian->achievement_nilai : 0;

    return $achievement;
}

function getGAPToSTDSbc($user_id,$date){
	$std = getSTD();
	$persentase = getPercentage(getMTDAch($user_id,$date), getTragetSales($user_id));

	$gapt_to_std = $persentase - $std;
    return $gapt_to_std;
}

function getAchDate($user_id,$date){
    $CI =& get_instance();

    $CI->load->model('AchievementModel');
    $pencapaian = $CI->AchievementModel->select(array('date' => $date,'user_id' => $user_id));
    $achievement =  (!empty($pencapaian)) ? $pencapaian->nilai : 0;

    return $achievement;
}
