<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <?php if ($this->session->flashdata('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12">
                    <form action="<?php echo site_url('index.php/manajemen/department/edit/'.$department->id) ?>" method="post" enctype="multipart/form-data" >
                    <input type="hidden" name="id" value="<?php echo $department->id?>" />
                        <div class="card">
                            <div class="card-header">
                                Edit Department
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="nama">Kategori*</label>
                                    <select name="kategori_id" class="form-control <?php echo form_error('kategori_id') ? 'is-invalid':'' ?>">
                                        <option value="">Pilih Kategori</option>
                                        <?php 
                                            foreach($kategori as $val){
                                        ?>
                                            <option value="<?php echo $val->id; ?>" <?php echo ($department->kategori_id == $val->id) ? 'selected' : '' ?>><?php echo $val->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="invalid-feedback">
                                        <?php echo form_error('kategori_id') ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama">Nama Department*</label>
                                    <input class="form-control <?php echo form_error('nama') ? 'is-invalid':'' ?>" type="text" name="nama" placeholder="Nama Department" value="<?php echo $department->nama ?>" />
                                    <div class="invalid-feedback">
                                        <?php echo form_error('nama') ?>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-dot-circle-o"></i> Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>