<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <?php if ($this->session->flashdata('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12">
                    <form action="<?php echo site_url('index.php/department/edit/'.$department->id) ?>" method="post" enctype="multipart/form-data" >
                    <input type="hidden" name="id" value="<?php echo $department->id?>" />
                        <div class="card">
                            <div class="card-header">
                                Edit Department
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="kode">Kode Department*</label>
                                    <input class="form-control <?php echo form_error('kode') ? 'is-invalid':'' ?>" type="text" name="kode" placeholder="Kode Department" value="<?php echo $department->kode ?>" />
                                    <div class="invalid-feedback">
                                        <?php echo form_error('kode') ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama">Nama Department*</label>
                                    <input class="form-control <?php echo form_error('nama') ? 'is-invalid':'' ?>" type="text" name="nama" placeholder="Nama Department" value="<?php echo $department->nama ?>" />
                                    <div class="invalid-feedback">
                                        <?php echo form_error('nama') ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="luasan_area">Luasan Area*</label>
                                    <input class="form-control <?php echo form_error('luasan_area') ? 'is-invalid':'' ?>" type="number" name="luasan_area" placeholder="Luasan Area" value="<?php echo $department->luasan_area ?>" />
                                    <div class="invalid-feedback">
                                        <?php echo form_error('luasan_area') ?>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-dot-circle-o"></i> Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
