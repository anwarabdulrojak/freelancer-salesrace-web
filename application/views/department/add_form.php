<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <?php if ($this->session->flashdata('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12">
                    <form action="<?php echo site_url('index.php/department/add') ?>" method="post" enctype="multipart/form-data" >
                        <div class="card">
                            <div class="card-header">
                                Tambah Department
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="kode">Kode Department*</label>
                                    <input class="form-control <?php echo form_error('kode') ? 'is-invalid':'' ?>"
                                        type="text" name="kode" placeholder="Kode Department" />
                                    <div class="invalid-feedback">
                                        <?php echo form_error('kode') ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama">Nama Department*</label>
                                    <input class="form-control <?php echo form_error('nama') ? 'is-invalid':'' ?>"
                                        type="text" name="nama" placeholder="Nama Department" />
                                    <div class="invalid-feedback">
                                        <?php echo form_error('nama') ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="luasan_area">Luasan Area*</label>
                                    <input class="form-control <?php echo form_error('luasan_area') ? 'is-invalid':'' ?>"
                                        type="text" name="luasan_area" placeholder="Luasan Area" />
                                    <div class="invalid-feedback">
                                        <?php echo form_error('luasan_area') ?>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-dot-circle-o"></i> Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
