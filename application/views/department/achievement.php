<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <?php if ($this->session->flashdata('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            Department Achievement
                        </div>
                        <div class="card-body">
                            <div class="table-responsive m-b-30">
                                <table class="table table-bordered table-data3">
                                    <thead>
                                        <tr>
                                            <th>Kode</th>
                                            <th>Department</th>
                                            <th>Target</th>
                                            <th>Sales Achievement</th>
                                            <th>%</th>
                                            <th>Gap To STD</th>
                                            <th>Prod MPP</th>
                                            <th>Prod Area</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            if(!empty($data)){ 
                                                foreach($data as $key => $value){ 
                                        ?>
                                            <tr>
                                                <td><?php echo $value->kode; ?></td>
                                                <td><?php echo $value->nama; ?></td>
                                                <td><?php echo formatNumber(getTargetDept($value->id)); ?></td>
                                                <td><?php echo formatNumber(getAchSalesDept($value->id)); ?></td>
												<td><?php echo getPercentage(getAchSalesDept($value->id), getTargetDept($value->id)); ?>%</td>
												<?php
													$gapt_to_std = getGAPToSTD($value->id);
												?>
												<td class="<?php echo ($gapt_to_std > 0) ? 'process' : 'denied'; ?>"><?php echo ($gapt_to_std > 0) ? '+ '.$gapt_to_std : $gapt_to_std; ?>%</td>
                                                <td><?php echo getProdMPP($value->id); ?></td>
                                                <td><?php echo getProdArea($value->id,$value->luasan_area); ?></td>
                                            </tr>
                                        <?php } }else{ ?>
                                            <tr>
                                                <td colspan="3">Data tidak ditemukan</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function deleteConfirm(url){
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }
</script>
<!-- Logout Delete Confirmation-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Data yang dihapus tidak akan bisa dikembalikan.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary btn-sm" type="button" data-dismiss="modal">Cancel</button>
                <a id="btn-delete" class="btn btn-danger btn-sm" href="#">Delete</a>
            </div>
        </div>
    </div>
</div>
