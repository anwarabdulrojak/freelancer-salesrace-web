<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <?php if ($this->session->flashdata('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12">
                    <form method="post" action="<?php echo base_url('index.php/sbc/save'); ?>">
                        <div class="card">
                            <div class="card-header">
                                Sales Book Control
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Tanggal</label>
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fas fa-calendar-alt"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="datepicker" name="date" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Achievement</label>
                                    <input type="number" class="form-control" name="nilai" placeholder="Achievement" required>
                                </div>
                                <input type="hidden" name="user_id" value="<?php echo $this->session->userdata('user_id') ?>">
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-dot-circle-o"></i> Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
