<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <?php if ($this->session->flashdata('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
							Sales Book Control <a href="<?php echo site_url('index.php/sbc/add') ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah</a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive m-b-30">
                                <table class="table table-bordered table-data3">
                                    <thead>
                                        <tr>
                                            <th>Tanggal</th>
                                            <th>Target</th>
                                            <th>Today Ach</th>
                                            <th>MTD Ach</th>
                                            <th>%</th>
                                            <th>GAP To STD</th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php 
											$start = new DateTime(date('Y-m-d'));
											$end = new DateTime(date('Y-m-t', strtotime(date('Y-m'))));
											$end = $end->modify('+1 day'); 
											
											$diff = DateInterval::createFromDateString('1 day');
											$periodStart = new DatePeriod($start, $diff, $end);
											
											foreach ( $periodStart as $dayDate ){
												$date = $dayDate->format( "Y-m-d");
												$user_id = $this->session->userdata('user_id');
                                        ?>
                                            <tr>
                                                <td><?php echo formatTanggal($date); ?></td>
                                                <td><?php echo formatNumber(getTragetSales($user_id)); ?></td>
                                                <td><?php echo formatNumber(getAchDate($user_id,$date)); ?></td>
                                                <td><?php echo formatNumber(getMTDAch($user_id,$date)); ?></td>
                                                <td><?php echo getPercentage(getMTDAch($user_id,$date), getTragetSales($user_id)); ?> %</td>
                                                <?php
													$gapt_to_std = getGAPToSTDSbc($user_id,$date);
												?>
												<td class="<?php echo ($gapt_to_std > 0) ? 'process' : 'denied'; ?>"><?php echo ($gapt_to_std > 0) ? '+ '.$gapt_to_std : $gapt_to_std; ?> %</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
