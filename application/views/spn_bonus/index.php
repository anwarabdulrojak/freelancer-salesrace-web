<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row mb-3">
                <div class="col-md-12">
                    <div class="overview-wrap">
                        <h3 class="title-2">SPN Bonus</h3>
                    </div>
                </div>
            </div>
            <?php
                $data_store = get_data_store();
                $jumlah_mpp = get_mpp_sales();
                $poin_sales = get_poin_sales();
                $persen_insentif_per_level = get_persen_insentif_per_level();
                $nilai_insentif_per_level = ($persen_insentif_per_level * $data_store['nilai_insentif'] ) / 100;
                $nilai_insentif_per_point = ($nilai_insentif_per_level > 0) ? $nilai_insentif_per_level / $jumlah_mpp : 0;
            ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Store</h4>
                            Target Store : Rp. <?php echo number_format($data_store['target_store'],0,',','.'); ?><br/>
                            Pencapaian Store : Rp. <?php echo number_format($data_store['pencapaian_store'],0,',','.'); ?><br/>
                            Persentase pencapaian Store : <?php echo number_format($data_store['pencapaian_store_persen'],2,',','.'); ?>%<br/>
                            Persentase maksimal total insentif : <?php echo number_format($data_store['nilai_insentif_persen'],2,',','.'); ?>%<br/>
                            Nilai maksimal total insentif : Rp. <?php echo number_format($data_store['nilai_insentif'],0,',','.'); ?><br/>
                            
                            <h4 class="mt-3">Sales</h4>
                            Bobot poin : 1.5 <br/>
                            Jumlah MPP : <?php echo $jumlah_mpp; ?><br/>
                            Jumlah poin sales : <?php echo $poin_sales; ?><br/>
                            Persentase insentif per Level : <?php echo number_format($persen_insentif_per_level,2,',','.'); ?>%<br/>
                            Nilai insentif per Level : Rp. <?php echo number_format($nilai_insentif_per_level,0,',','.'); ?><br/>
                            Nilai insentif per point : Rp. <?php echo number_format($nilai_insentif_per_point,0,',','.'); ?><br/>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>Nama</th>
                                            <th>Poin Individu</th>
                                            <th>Nilai Insentif</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                        if(!empty($data)){
                                            foreach($data as $key => $value){ 
                                                $CI =& get_instance();                    
                                                $CI->load->model('DepartmentModel');
                                                $kategori_id = $CI->DepartmentModel->select(array('id'=>$value->department_id))->kategori_id;
                                                $poin_individu = get_poin_individu($value->id, $kategori_id);
                                                $nilai_insentif = $poin_individu * $nilai_insentif_per_point;
                                        ?>
                                            <tr>
                                                <td><?php echo $key+1; ?></td>
                                                <td><?php echo $value->nama; ?></td>
                                                <td><?php print_r($poin_individu); ?></td>
                                                <td>Rp. <?php echo number_format($nilai_insentif,0,',','.'); ?></td>
                                            </tr>
                                        <?php } }else{ ?>
                                            <tr>
                                                <td colspan="4">Data tidak ditemukan</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>