<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row mb-3">
                <div class="col-md-12">
                    <div class="overview-wrap">
                        <h3 class="title-2">Sales Race</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="default-tab">
                                <nav>
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        <a class="nav-item nav-link active" id="nav-week-tab" data-toggle="tab" href="#nav-week" role="tab" aria-controls="nav-week" aria-selected="true">By Week</a>
                                        <a class="nav-item nav-link" id="nav-month-tab" data-toggle="tab" href="#nav-month" role="tab" aria-controls="nav-month" aria-selected="false">By Month</a>
                                    </div>
                                </nav>
                                <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-week" role="tabpanel" aria-labelledby="nav-week-tab">
                                    <?php if(!empty($week)){
                                        foreach($week as $key => $value){ 
                                            $min = 0;
                                            $max = 1000;
                                            $now = !empty($value->pencapaian) ? $value->pencapaian : 0;
                                            $siz = ($now-$min)*100/($max-$min);
                                        ?>
                                        
                                            <div class="email__item">
                                                <div class="image img-cir img-40">
                                                    <img src="<?php echo base_url('upload/user/'.$value->user_photo) ?>" alt="user img"/>
                                                </div>
                                                <div class="content">
                                                    <p><?php echo $value->user_nama; ?></p>
                                                    <div class="progress">
                                                        <div class="progress-bar bg-success" style="width: <?php echo $siz ?>%"  aria-valuenow="<?php echo $now ?>" aria-valuemin="<?php echo $min ?>" aria-valuemax="<?php echo $max ?>"></div>
                                                    </div>
                                                    <?php echo number_format($now,2,',','.') ?>%
                                                </div>
                                            </div>
                                        <?php } }else{ ?>
                                            Tidak Ada Data
                                        <?php } ?>
                                    </div>
                                    <div class="tab-pane fade" id="nav-month" role="tabpanel" aria-labelledby="nav-month-tab">
                                        <?php if(!empty($month)){
                                            foreach($month as $key => $value){ 
                                                $min = 0;
                                                $max = 1000;
                                                $now = !empty($value->pencapaian) ? $value->pencapaian : 0;
                                                $siz = ($now-$min)*100/($max-$min);
                                        ?>
                                            <div class="email__item">
                                                <div class="image img-cir img-40">
                                                    <img src="<?php echo base_url('upload/user/'.$value->user_photo) ?>" alt="user img"/>
                                                </div>
                                                <div class="content">
                                                    <p><?php echo $value->user_nama; ?></p>
                                                    <div class="progress mb-3">
                                                        <div class="progress-bar bg-success" role="progressbar"  style="width: <?php echo $siz ?>%"  aria-valuenow="<?php echo $now ?>" aria-valuemin="<?php echo $min ?>" aria-valuemax="<?php echo $max ?>"></div>
                                                    </div>
                                                    <p><?php echo number_format($now,2,',','.') ?>%</p>
                                                </div>
                                            </div>
                                        <?php } }else{ ?>
                                            Tidak Ada Data    
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
