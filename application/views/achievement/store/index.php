<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row mb-3">
                <div class="col-md-12">
                    <div class="overview-wrap">
                        <h3 class="title-2">Set Achievement Store</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-lg-6">
                    <div class="overview-item overview-item--c4">
                        <div class="overview__inner">
                            <div class="overview-box clearfix">
                                <div class="icon">
                                    <i class="zmdi zmdi-money"></i>
                                </div>
                                <div class="text">
                                    <h2>Rp. <?php echo !empty($target) ? number_format($target->nilai,0,',','.') : 0 ?></h2>
                                    <span>Target Bulan <?php echo $month; ?> Tahun <?php echo $year; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <form method="post" action="<?php echo base_url('index.php/achievement/store/save'); ?>">
                        <div class="card">
                            <div class="card-header">Set Achievement Store</div>
                            <div class="card-body">
                                <?php 
                                    $CI =& get_instance();
                                    $CI->load->model('AchievementStoreModel');
                                    $where = array('MONTH(date)'=> date('m'),'YEAR(date)'=> date('Y'));
                                    $cek = $CI->AchievementStoreModel->select($where);
                                ?>
                                <div class="form-group">
                                    <label class="control-label mb-1">Achievement Store</label>
                                    <input type="number" class="form-control" name="nilai" value="<?php echo !empty($cek) ? $cek->nilai : null ?>" placeholder="Achievement" required>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-dot-circle-o"></i> Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>