<!-- MENU SIDEBAR-->
<aside class="menu-sidebar d-none d-lg-block">
    <div class="logo">
        <h3>Sales Race</h3>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">
        <nav class="navbar-sidebar">
            <ul class="list-unstyled navbar__list">
                <li class="<?php echo ($this->uri->segment(2) == 'home') ? 'active' : ''?>">
                    <a class="js-arrow" href="<?php echo base_url('index.php/page/home'); ?>">
                        <i class="fas fa-tachometer-alt"></i>Dashboard
                    </a>
                </li>
                <?php if($this->session->userdata('role') == 'manajer'){ ?>
                <li class="has-sub <?php echo ($this->uri->segment(1) == 'set_target') ? 'active' : ''?>">
                    <a class="js-arrow" href="#"><i class="fas fa-bullseye"></i>Set Target</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li class="<?php echo  ($this->uri->segment(1) == 'set_target') && ($this->uri->segment(2) == 'store') ? 'active' : ''?>">
                            <a href="<?php echo base_url('index.php/set_target/store'); ?>"> Store</a>
                        </li>
                        <li class="<?php echo ($this->uri->segment(1) == 'set_target') && ($this->uri->segment(2) == 'department') ? 'active' : ''?>">
                            <a href="<?php echo base_url('index.php/set_target/department'); ?>"> Department</a>
                        </li>
                        <li class="<?php echo ($this->uri->segment(1) == 'set_target') && ($this->uri->segment(2) == 'sales_executive') ? 'active' : ''?>">
                            <a href="<?php echo base_url('index.php/set_target/sales_executive'); ?>"> Sales Executive</a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub <?php echo ($this->uri->segment(1) == 'department') ? 'active' : ''?>">
                    <a class="js-arrow" href="#"><i class="fas fa-user"></i>Department</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li class="<?php echo  ($this->uri->segment(1) == 'department') && ($this->uri->segment(2) == 'data') ? 'active' : ''?>">
                            <a href="<?php echo base_url('index.php/department/data'); ?>"> Data Department</a>
                        </li>
                        <li class="<?php echo ($this->uri->segment(1) == 'department') && ($this->uri->segment(2) == 'achievement') ? 'active' : ''?>">
                            <a href="<?php echo base_url('index.php/department/achievement'); ?>"> Achievement</a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub <?php echo ($this->uri->segment(1) == 'sales_executive') ? 'active' : ''?>">
                    <a class="js-arrow" href="#"><i class="fas fa-user"></i>Sales Executive</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li class="<?php echo  ($this->uri->segment(1) == 'sales_executive') && ($this->uri->segment(2) == 'data') ? 'active' : ''?>">
                            <a href="<?php echo base_url('index.php/sales_executive/data'); ?>"> Data Sales Executive</a>
                        </li>
                        <li class="<?php echo ($this->uri->segment(1) == 'sales_executive') && ($this->uri->segment(2) == 'achievement') ? 'active' : ''?>">
                            <a href="<?php echo base_url('index.php/sales_executive/achievement'); ?>"> Achievement</a>
                        </li>
                    </ul>
                </li>
                <?php }else{ ?>
                <li class="<?php echo ($this->uri->segment(1) == 'sbc') ? 'active' : ''?>">
                    <a href="<?php echo base_url('index.php/sbc'); ?>">
                        <i class="fas fa-file"></i> Sales Book Control
                    </a>
                </li>
                <?php } ?>
            </ul>
        </nav>
    </div>
</aside>
