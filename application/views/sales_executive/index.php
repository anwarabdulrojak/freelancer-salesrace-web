<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <?php if ($this->session->flashdata('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
						Sales Executive Data <a href="<?php echo site_url('index.php/sales_executive/add') ?>" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah</a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive m-b-30">
                                <table class="table table-bordered table-data3">
                                    <thead>
                                        <tr>
                                            <th>NIP</th>
                                            <th>Nama</th>
                                            <th>Department</th>
                                            <th>Target</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            if(!empty($data)){ 
                                                foreach($data as $key => $value){ 
                                        ?>
                                            <tr>
                                                <td><?php echo $value->nip; ?></td>
                                                <td><?php echo $value->nama; ?></td>
                                                <td><?php echo getDept($value->department_id); ?></td>
                                                <td><?php echo formatNumber(getTragetSales($value->id)); ?></td>
                                                <td>
                                                    <a href="<?php echo site_url('index.php/sales_executive/edit/'.$value->id) ?>" class="btn btn-success btn-sm">
                                                        <i class="fa fa-edit"></i> Edit
                                                    </a>
                                                    <a onclick="deleteConfirm('<?php echo site_url('index.php/sales_executive/delete/'.$value->id) ?>')" href="#!" class="btn btn-danger btn-sm">
                                                        <i class="fa fa-trash"></i> Hapus
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php } }else{ ?>
                                            <tr>
                                                <td colspan="3">Data tidak ditemukan</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function deleteConfirm(url){
        $('#btn-delete').attr('href', url);
        $('#deleteModal').modal();
    }
</script>
<!-- Logout Delete Confirmation-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Data yang dihapus tidak akan bisa dikembalikan.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary btn-sm" type="button" data-dismiss="modal">Cancel</button>
                <a id="btn-delete" class="btn btn-danger btn-sm" href="#">Delete</a>
            </div>
        </div>
    </div>
</div>
