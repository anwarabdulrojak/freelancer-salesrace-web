<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <?php if ($this->session->flashdata('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-lg-12">
                    <form action="<?php echo site_url('index.php/sales_executive/edit/'.$user->id) ?>" method="post" enctype="multipart/form-data" >
                    <input type="hidden" name="id" value="<?php echo $user->id?>" />
                        <div class="card">
                            <div class="card-header">
                                Edit Sales Executive
                            </div>
                            <div class="card-body">
							<div class="form-group">
                                    <label for="nama">Department*</label>
                                    <select name="department_id" class="form-control <?php echo form_error('department_id') ? 'is-invalid':'' ?>">
                                        <option value="">Pilih Department</option>
                                        <?php 
                                            foreach($department as $val){
                                        ?>
                                            <option value="<?php echo $val->id; ?>" <?php echo ($user->department_id == $val->id) ? 'selected' : '' ?>><?php echo $val->nama; ?></option>
                                        <?php } ?>
                                    </select>
                                    <div class="invalid-feedback">
                                        <?php echo form_error('department_id') ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nip">NIP*</label>
                                    <input class="form-control <?php echo form_error('nip') ? 'is-invalid':'' ?>" type="text" name="nip" placeholder="NIP" value="<?php echo $user->nip ?>"/>
                                    <div class="invalid-feedback">
                                        <?php echo form_error('nip') ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="nama">Nama User*</label>
                                    <input class="form-control <?php echo form_error('nama') ? 'is-invalid':'' ?>" type="text" name="nama" placeholder="Nama User" value="<?php echo $user->nama ?>"/>
                                    <div class="invalid-feedback">
                                        <?php echo form_error('nama') ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="password">Password*</label>
                                    <input class="form-control <?php echo form_error('password') ? 'is-invalid':'' ?>" type="password" name="password" placeholder="Password" />
                                    <div class="invalid-feedback">
                                        <?php echo form_error('password') ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">Photo</label>
                                    <input class="form-control-file <?php echo form_error('photo') ? 'is-invalid':'' ?>" type="file" name="photo" />
                                    <input type="hidden" name="old_photo" value="<?php echo $user->photo?>">
                                    <div class="invalid-feedback">
                                        <?php echo form_error('photo') ?>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-dot-circle-o"></i> Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
