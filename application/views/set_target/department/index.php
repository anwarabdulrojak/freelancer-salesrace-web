<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row mb-3">
                <div class="col-md-12">
                    <div class="overview-wrap">
                        <h3 class="title-2">Set Target Department</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
					<?php echo $this->session->flashdata('notif') ?>
                </div>
                <div class="col-lg-6">
                    <form method="post" action="<?php echo base_url('index.php/set_target/department/save'); ?>">
                        <div class="card">
                            <div class="card-header">Set Target Department</div>
                            <div class="card-body">
								<div class="form-group">
                                    <label class="control-label mb-1">Department</label>
									<select class="form-control" name="department_id" required>
										<option value="">Pilih Department</option>
										<?php foreach ($department as $value) { ?>
											<option value="<?php echo $value->id; ?>"><?php echo $value->kode.' - '.$value->nama; ?></option>
										<?php } ?>
									</select>
                                </div>
								<div class="form-group">
                                    <label class="control-label mb-1">Tahun</label>
									<select class="form-control" name="year" required>
										<option value="">Pilih Tahun</option>
										<?php foreach (range(date('Y', strtotime('+1 year')), '2000') as $x) { ?>
											<option value="<?php echo $x; ?>"><?php echo $x; ?></option>
										<?php } ?>
									</select>
                                </div>
                                <div class="form-group">
									<label class="control-label mb-1">Bulan</label>
									<select class="form-control" name="month" required>
										<option value="">Pilih Bulan</option>
										<option value="01">Januari</option>
										<option value="02">Februari</option>
										<option value="03">Maret</option>
										<option value="04">April</option>
										<option value="05">Mei</option>
										<option value="06">Juni</option>
										<option value="07">Juli</option>
										<option value="08">Agustus</option>
										<option value="09">September</option>
										<option value="10">Oktober</option>
										<option value="11">November</option>
										<option value="12">Desember</option>
									</select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label mb-1">Target</label>
                                    <input type="number" class="form-control" name="nilai" placeholder="Target" required>
								</div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-dot-circle-o"></i> Submit</button>
                            </div>
                        </div>
                    </form>
				</div>
				<div class="col-lg-6">
					<form method="POST" action="<?php echo base_url('index.php/set_target/department/import'); ?>" enctype="multipart/form-data">
						<div class="card">
                            <div class="card-header">Import Target Department</div>
                            <div class="card-body">
								<div class="form-group">
									<label for="exampleInputEmail1">UNGGAH FILE EXCEL</label>
									<input type="file" name="userfile" class="form-control" required>
								</div>
							</div>
							<div class="card-footer">
								<button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-dot-circle-o"></i> Import</button>
							</div>
						</div>
					</form>
				</div>
            </div>
        </div>
    </div>
</div>
