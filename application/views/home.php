<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
				<div class="col-md-12">
					<div class="overview-wrap">
						<h2 class="title-1"><?php echo formatTanggal(); ?> Overview</h2>
					</div>
				</div>
			</div>
			<div class="row m-t-25">
				<div class="col-sm-6 col-lg-3">
					<div class="overview-item overview-item--c1">
						<div class="overview__inner">
							<div class="overview-box clearfix">
								<div class="icon">
									<i class="zmdi zmdi-account-o"></i>
								</div>
								<br/>
								<div class="text">
									<h2><?php echo getSTD(); ?> %</h2>
									<span>STD</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php if($this->session->userdata('role') == 'manajer'){ ?>
				<div class="col-sm-6 col-lg-3">
					<div class="overview-item overview-item--c2">
						<div class="overview__inner">
							<div class="overview-box clearfix">
								<div class="icon">
									<i class="zmdi zmdi-shopping-cart"></i>
								</div>
								<br/>
								<div class="text">
									<h2><?php echo formatNumber(getTargetStore()); ?></h2>
									<span>Target</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="overview-item overview-item--c3">
						<div class="overview__inner">
							<div class="overview-box clearfix">
								<div class="icon">
									<i class="zmdi zmdi-calendar-note"></i>
								</div>
								<br/>
								<div class="text">
									<h2><?php echo formatNumber(getSalesAch()); ?></h2>
									<span>Sales Ach</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="overview-item overview-item--c4">
						<div class="overview__inner">
							<div class="overview-box clearfix">
								<div class="icon">
									<i class="zmdi zmdi-money"></i>
								</div>
								<br/>
								<div class="text">
									<h2><?php echo getPercentage(getSalesAch(),getTargetStore()); ?> %</h2>
									<span>Percentage</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php }else{ ?>
				<div class="col-sm-6 col-lg-3">
					<div class="overview-item overview-item--c2">
						<div class="overview__inner">
							<div class="overview-box clearfix">
								<div class="icon">
									<i class="zmdi zmdi-shopping-cart"></i>
								</div>
								<br/>
								<div class="text">
									<h2><?php echo formatNumber(getTragetSales($this->session->userdata('user_id'))); ?></h2>
									<span>Target</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="overview-item overview-item--c3">
						<div class="overview__inner">
							<div class="overview-box clearfix">
								<div class="icon">
									<i class="zmdi zmdi-calendar-note"></i>
								</div>
								<br/>
								<div class="text">
									<h2><?php echo formatNumber(getAchSales($this->session->userdata('user_id'))); ?></h2>
									<span>Sales Ach</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-lg-3">
					<div class="overview-item overview-item--c4">
						<div class="overview__inner">
							<div class="overview-box clearfix">
								<div class="icon">
									<i class="zmdi zmdi-money"></i>
								</div>
								<br/>
								<div class="text">
									<h2><?php echo getPercentage(getAchSales($this->session->userdata('user_id')),getTragetSales($this->session->userdata('user_id'))); ?> %</h2>
									<span>Percentage</span>
								</div>
							</div>
						</div>
					</div>
				</div>
                <?php } ?>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive m-b-40">
						<table class="table table-borderless table-data3">
							<thead>
								<tr>
									<th>Kode</th>
									<th>Department</th>
									<th>Target</th>
									<th>Sales Ach</th>
									<th>Percentage</th>
									<th>Gap to STD</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									if(!empty($department)){ 
										foreach($department as $key => $value){
								?>
									<tr>
										<td><?php echo $value->kode; ?></td>
										<td><?php echo $value->nama; ?></td>
										<td><?php echo formatNumber(getTargetDept($value->id)); ?></td>
										<td><?php echo formatNumber(getAchSalesDept($value->id)); ?></td>
										<td><?php echo getPercentage(getAchSalesDept($value->id), getTargetDept($value->id)); ?>%</td>
										<?php
											$gapt_to_std = getGAPToSTD($value->id);
										?>
										<td class="<?php echo ($gapt_to_std > 0) ? 'process' : 'denied'; ?>"><?php echo ($gapt_to_std > 0) ? '+ '.$gapt_to_std : $gapt_to_std; ?>%</td>
									</tr>
								<?php } }else{ ?>
									<tr>
										<td colspan="6">Data tidak ditemukan</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>
