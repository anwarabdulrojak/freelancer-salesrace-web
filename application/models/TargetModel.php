<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TargetModel extends CI_Model {
    
    public function get(){
        $query = $this->db->get('target');
        $result=$query->result();
        
        return $result;
    }
    
    public function insert($data){
        return $this->db->insert('target', $data);
    }
    
    public function update($where,$data){
		$this->db->where($where);
        return $this->db->update('target',$data);
	}

    public function select($where){
        $this->db->where($where); 
        $result = $this->db->get('target')->row();

        return $result;
    }
    
    public function save_batch($data){
        return $this->db->insert_batch('target', $data);
    }
}