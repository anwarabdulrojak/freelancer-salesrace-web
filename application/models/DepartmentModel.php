<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DepartmentModel extends CI_Model {

    private $_table = "department";

    public $id;
    public $nama;
    public $kode;
    public $luasan_area;
    public $kategori_id;
    
    public function rules()
    {
        return [
            [
                'field' => 'kode',
                'label' => 'Kode',
                'rules' => 'required'
			],
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
			],
            [
                'field' => 'luasan_area',
                'label' => 'Luasan Area',
                'rules' => 'required'
            ]
        ];
    }
    
    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->nama = $post["nama"];
        $this->kode = $post["kode"];
        $this->luasan_area = $post["luasan_area"];
        $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->id = $post['id'];
        $this->nama = $post["nama"];
        $this->kode = $post["kode"];
        $this->luasan_area = $post["luasan_area"];
        $this->db->update($this->_table, $this, array('id' => $post['id']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id" => $id));
    }
    
    public function get(){
        $query = $this->db->get('department');
        $result=$query->result();
        
        return $result;
    }

    public function select($where){
        $this->db->where($where); 
        $result = $this->db->get('department')->row();

        return $result;
    }
    
    public function save_batch($data){
        return $this->db->insert_batch('department', $data);
    }
}
