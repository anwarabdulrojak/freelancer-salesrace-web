<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TargetStoreModel extends CI_Model {
    
    public function get(){
        $query = $this->db->get('target_store');
        $result=$query->result();
        
        return $result;
    }
    
    public function insert($data){
        return $this->db->insert('target_store', $data);
    }
    
    public function update($where,$data){
		$this->db->where($where);
        return $this->db->update('target_store',$data);
	}

    public function select($where){
        $this->db->where($where); 
        $result = $this->db->get('target_store')->row();

        return $result;
    }
    
    public function save_batch($data){
        return $this->db->insert_batch('target_store', $data);
    }
}