<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AchievementStoreModel extends CI_Model {
    
    public function get(){
        $query = $this->db->get('achievement_store');
        $result=$query->result();
        
        return $result;
    }
    
    public function insert($data){
        return $this->db->insert('achievement_store', $data);
    }
    
    public function update($where,$data){
		$this->db->where($where);
        return $this->db->update('achievement_store',$data);
	}

    public function select($where){
        $this->db->where($where); 
        $result = $this->db->get('achievement_store')->row();

        return $result;
    }
    
    public function save_batch($data){
        return $this->db->insert_batch('achievement_store', $data);
    }
}