<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UserModel extends CI_Model {

    private $_table = "user";

    public $id;
    public $department_id;
    public $nip;
    public $nama;
    public $password;
    public $photo = 'default.png';
    public $role;
    
    public function rules()
    {
        return [
            [
                'field' => 'nip',
                'label' => 'NIP',
                'rules' => 'required'
            ],
            [
                'field' => 'nama',
                'label' => 'Nama',
                'rules' => 'required'
            ],
            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required'
            ]
        ];
    }
    
    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->department_id = $post["department_id"];
        $this->nip = $post["nip"];
        $this->nama = $post["nama"];
        $this->password = md5($post["password"]);
        $this->role = 2;
        $this->photo = $this->_uploadImage();
        $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $post = $this->input->post();
        $this->id = $post['id'];
        if ($post["department_id"] != null) {
            $this->department_id = $post["department_id"];
        }
        $this->nip = $post["nip"];
        $this->nama = $post["nama"];
        $this->password = md5($post["password"]);
        $this->role = 2;
        if (!empty($_FILES["photo"]["name"])) {
            $this->photo = $this->_uploadImage();
        } else {
            $this->photo = $post["old_photo"];
        }
        $this->db->update($this->_table, $this, array('id' => $post['id']));
    }

    public function delete($id)
    {
        $this->_deleteImage($id);
        return $this->db->delete($this->_table, array("id" => $id));
    }
    
    private function _uploadImage()
    {
        $config['upload_path']          = './upload/user/';
        $config['allowed_types']        = 'jpg|png';
        $config['file_name']            = $this->id;
        $config['overwrite']			= true;
        $config['max_size']             = 1024; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;
    
        $this->load->library('upload', $config);
    
        if ($this->upload->do_upload('photo')) {
            return $this->upload->data("file_name");
        }
        
        return "default.jpg";
    }
    
    private function _deleteImage($id)
    {
        $user = $this->getById($id);
        if ($user->photo != "default.jpg") {
            $filename = explode(".", $user->photo)[0];
            return array_map('unlink', glob(FCPATH."upload/user/$filename.*"));
        }
    }
    
    public function get($nip){
        $this->db->where('nip', $nip); // Untuk menambahkan Where Clause : nip='$nip'
        $result = $this->db->get('user')->row(); // Untuk mengeksekusi dan mengambil data hasil query

        return $result;
    }
    
    public function get_all_sales(){
        $this->db->where('role', 2);// Untuk menambahkan Where Clause : nip='$nip'
        $result = $this->db->get('user')->result(); // Untuk mengeksekusi dan mengambil data hasil query

        return $result;
    }

    public function select($where){
        $this->db->where($where); 
        $result = $this->db->get('user')->row();

        return $result;
    }

    public function num_rows($where){
        $this->db->where($where); 
        $result = $this->db->get('user')->num_rows();

        return $result;
    }
    
    public function getSalesRaceWeek(){        
        $monday = strtotime("last monday");
        $monday = date('w', $monday)==date('w') ? $monday+7*86400 : $monday;
        $sunday = strtotime(date("Y-m-d",$monday)." +6 days");

        $start_date = date("Y-m-d",$monday);
        $end_date = date("Y-m-d",$sunday);

        $SQL = "SELECT *,user.nama as user_nama,user.photo as user_photo, achievement_nilai / target_nilai * 100 as pencapaian 
        FROM user JOIN department on department.id = user.department_id
        JOIN kategori ON department.kategori_id = kategori.id 
        JOIN 
            (SELECT target.kategori_id, SUM(target.nilai) AS target_nilai FROM target WHERE start_date = '".$start_date."' AND end_date = '".$end_date."' GROUP BY kategori_id) t1
        ON t1.kategori_id = kategori.id
        LEFT JOIN 
            (SELECT achievement.user_id, SUM(achievement.nilai) AS achievement_nilai FROM achievement WHERE date BETWEEN '".$start_date."' AND '".$end_date."' GROUP BY user_id) t2
        ON t2.user_id = user.id
        WHERE user.role = '2'
        ORDER by pencapaian DESC";
        $query = $this->db->query($SQL);

        return $query->result();
    }
    
    public function getSalesRaceMonth(){
        $SQL = "SELECT *,user.nama as user_nama,user.photo as user_photo, achievement_nilai / target_nilai * 100 as pencapaian 
        FROM user JOIN department on department.id = user.department_id
        JOIN kategori ON department.kategori_id = kategori.id 
        LEFT JOIN 
            (SELECT target.kategori_id, SUM(target.nilai) AS target_nilai FROM target WHERE '".date('m')."' BETWEEN MONTH(start_date) AND MONTH(end_date) GROUP BY kategori_id) t1
        ON t1.kategori_id = kategori.id
        LEFT JOIN 
            (SELECT achievement.user_id, SUM(achievement.nilai) AS achievement_nilai FROM achievement WHERE MONTH(date) = ".date('m')." GROUP BY user_id) t2
        ON t2.user_id = user.id
        WHERE user.role = '2'
        ORDER by pencapaian DESC";
        $query = $this->db->query($SQL);

        return $query->result();
    }
}
