<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AchievementModel extends CI_Model {
    
    public function get(){
        $query = $this->db->get('achievement');
        $result=$query->result();
        
        return $result;
    }
    
    public function getSbc($sales_id){
        $this->db->where("user_id = '".$sales_id."'");
        $this->db->where("date >= '".date("Y-m-d")."'");
		$this->db->order_by("date", "ASC");
		$query = $this->db->get('achievement');
        $result=$query->result();
        
        return $result;
    }
    
    public function insert($data){
        return $this->db->insert('achievement', $data);
    }
    
    public function update($where,$data){
		$this->db->where($where);
        return $this->db->update('achievement',$data);
	}

    public function select($where){
        $this->db->where($where); 
        $result = $this->db->get('achievement')->row();

        return $result;
    }
    
    public function save_batch($data){
        return $this->db->insert_batch('achievement', $data);
    }
    
    public function pencapaian($user_id, $kategori_id, $start_date, $end_date){
        $this->db->select('SUM(nilai) as nilai_ach');
        $this->db->from('achievement');   
        $this->db->where("date BETWEEN '$start_date' AND '$end_date'");
        $this->db->where("user_id",$user_id);

        $achievement = $this->db->get()->row();
        $achievement_nilai = (!empty($achievement)) ? $achievement->nilai_ach : 0;
        $target_nilai = $this->target($kategori_id, $start_date, $end_date);

        return ($achievement_nilai > 0) ? $achievement_nilai / $target_nilai * 100 : 0;
    }

    public function target($kategori_id, $start_date, $end_date)
    {
        $this->db->where('kategori_id', $kategori_id);
        $this->db->where('start_date', $start_date);
        $this->db->where('end_date', $end_date);
        
        $target = $this->db->get('target')->row();
        return (!empty($target)) ? $target->nilai : 0;
    }
    
    public function pencapaian_nilai_user($user_id, $start_date, $end_date){
        $this->db->select('SUM(nilai) as nilai_ach');
        $this->db->from('achievement');   
        $this->db->where("date BETWEEN '$start_date' AND '$end_date'");
        $this->db->where("user_id",$user_id);

        $achievement = $this->db->get()->row();
        return (!empty($achievement)) ? $achievement->nilai_ach : 0;
    }
    
    public function pencapaian_nilai($start_date, $end_date){
        $this->db->select('SUM(nilai) as nilai_ach');
        $this->db->from('achievement');   
        $this->db->where("date BETWEEN '$start_date' AND '$end_date'");

        $achievement = $this->db->get()->row();
        return (!empty($achievement)) ? $achievement->nilai_ach : 0;
    }
    
    public function achievement_sales($month, $year){
        $this->db->select('SUM(nilai) as nilai_ach');
        $this->db->from('achievement');   
        $this->db->where("date BETWEEN '$month' AND '$year'");

		$achievement = $this->db->get()->row();
        return (!empty($achievement->nilai_ach)) ? $achievement->nilai_ach : 0;
    }
    
    public function getAchSalesByDept($department_id){
        $SQL = "SELECT SUM(achievement.nilai) AS achievement_nilai FROM achievement WHERE MONTH(date) = ".date('m')." 
		AND user_id IN ( SELECT user_id FROM user WHERE user.department_id = '".$department_id."') GROUP BY user_id";
		
		$query = $this->db->query($SQL);

        return $query->result();
    }
    
    public function getAchSales($sales_id){
        $SQL = "SELECT SUM(achievement.nilai) AS achievement_nilai FROM achievement WHERE MONTH(date) = ".date('m')." 
		AND user_id = '".$sales_id."' GROUP BY user_id";
		
		$query = $this->db->query($SQL);

        return $query->row();
    }
    
    public function getMTDAch($sales_id,$date){
        $SQL = "SELECT SUM(achievement.nilai) AS achievement_nilai FROM achievement WHERE MONTH(date) = ".date('m')." 
		AND date <= '".$date."' AND user_id = '".$sales_id."' GROUP BY user_id";
		
		$query = $this->db->query($SQL);

        return $query->row();
    }
}
