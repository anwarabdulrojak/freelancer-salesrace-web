<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_executive extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(["UserModel","DepartmentModel"]);
        $this->load->library('form_validation');
    }

    public function data()
    {
        $data["data"] = $this->UserModel->get_all_sales();
        $data["title"] = 'Data Sales Executive';
        $this->render("sales_executive/index", $data);
    }

    public function add()
    {
        $user = $this->UserModel;
        $validation = $this->form_validation;
        $validation->set_rules($user->rules());

        if ($validation->run()) {
            $user->save();
            $this->session->set_flashdata('success', 'Data berhasil disimpan');
            redirect(site_url('index.php/sales_executive/data'));
        }
        $data["title"] = 'Tambah Department';
		$data["department"] = $this->DepartmentModel->getAll();
		
        $this->render("sales_executive/add_form", $data);
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('index.php/sales_executive/user');
       
        $user = $this->UserModel;
        $validation = $this->form_validation;
        $validation->set_rules($user->rules());

        if ($validation->run()) {
            $user->update();
            $this->session->set_flashdata('success', 'Data berhasil dirubah');
            redirect(site_url('index.php/sales_executive/data'));
        }

        $data["user"] = $user->getById($id);
        if (!$data["user"]) show_404();
        $data["title"] = 'Edit Department';
		$data["department"] = $this->DepartmentModel->getAll();
        
        $this->render("sales_executive/edit_form", $data);
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->UserModel->delete($id)) {
            $this->session->set_flashdata('success', 'Data berhasil dihapus');
            redirect(site_url('index.php/sales_executive/data'));
        }
    }

    public function achievement()
    {
        $data["data"] = $this->UserModel->get_all_sales();
        $data["title"] = 'Achievement Sales Executive';
        $this->render("sales_executive/achievement", $data);
    }
}
