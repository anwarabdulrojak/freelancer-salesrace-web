<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(["UserModel", "DepartmentModel"]);
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data["data"] = $this->UserModel->getAll();
        $data["title"] = 'User';
        $this->render("manajemen/user/index", $data);
    }

    public function add()
    {
        $user = $this->UserModel;
        $validation = $this->form_validation;
        $validation->set_rules($user->rules());

        if ($validation->run()) {
            $user->save();
            $this->session->set_flashdata('success', 'Data berhasil disimpan');
            redirect(site_url('index.php/manajemen/user'));
        }

        $data["title"] = 'Tambah User';
        $data["department"] = $this->DepartmentModel->getAll();
        $this->render("manajemen/user/add_form", $data);
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('index.php/manajemen/user');
       
        $user = $this->UserModel;
        $validation = $this->form_validation;
        $validation->set_rules($user->rules());

        if ($validation->run()) {
            $user->update();
            $this->session->set_flashdata('success', 'Data berhasil dirubah');
            redirect(site_url('index.php/manajemen/user'));
        }

        $data["user"] = $user->getById($id);
        if (!$data["user"]) show_404();
        $data["department"] = $this->DepartmentModel->getAll();
        $data["title"] = 'Edit User';
        
        $this->render("manajemen/user/edit_form", $data);
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->UserModel->delete($id)) {
            $this->session->set_flashdata('success', 'Data berhasil dihapus');
            redirect(site_url('index.php/manajemen/user'));
        }
    }
}
