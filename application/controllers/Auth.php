<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

  public function __construct(){
    parent::__construct();

    $this->load->model(['UserModel', 'DepartmentModel']);
  }

  public function index(){
    if($this->session->userdata('authenticated')) // Jika user sudah login (Session authenticated ditemukan)
      redirect('index.php/page/home'); // Redirect ke page home

    $this->load->view('login');
  }

  public function login(){
    $nip = $this->input->post('nip'); // Ambil isi dari inputan nip pada form login
    $password = md5($this->input->post('password')); // Ambil isi dari inputan password pada form login dan encrypt dengan md5

    $user = $this->UserModel->get($nip); // Panggil fungsi get yang ada di UserModel.php

    if(empty($user)){ // Jika hasilnya kosong / user tidak ditemukan
      $this->session->set_flashdata('message', 'NIP tidak ditemukan'); // Buat session flashdata
      redirect('index.php/auth'); // Redirect ke halaman login
    }else{
      if($password == $user->password){
        $kategori_id = null;
        if(!empty($user->department_id)){          
          $kategori_id = $this->DepartmentModel->select(array('id'=>$user->department_id))->kategori_id;
        }
        $session = array(
          'authenticated'=>true, // Buat session authenticated dengan value true
          'nip'=>$user->nip,  // Buat session nip
          'nama'=>$user->nama, // Buat session nama
          'photo'=>$user->photo, // Buat session nama
          'role'=>$user->role == 1 ? 'manajer' : 'sales', // Buat session role
          'user_id'=>$user->id,
          'kategori_id' => $kategori_id
        );

        $this->session->set_userdata($session); // Buat session sesuai $session
        redirect('index.php/page/home'); // Redirect ke halaman home
      }else{
        $this->session->set_flashdata('message', 'Password salah'); // Buat session flashdata
        redirect('index.php/auth'); // Redirect ke halaman login
      }
    }
  }

  public function logout(){
    $this->session->sess_destroy(); // Hapus semua session
    redirect('index.php/auth'); // Redirect ke halaman login
  }
}