<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model(["DepartmentModel"]);
        $this->load->library('form_validation');
    }

    public function data()
    {
        $data["data"] = $this->DepartmentModel->getAll();
        $data["title"] = 'Data Department';
        $this->render("department/index", $data);
    }

    public function add()
    {
        $department = $this->DepartmentModel;
        $validation = $this->form_validation;
        $validation->set_rules($department->rules());

        if ($validation->run()) {
            $department->save();
            $this->session->set_flashdata('success', 'Data berhasil disimpan');
            redirect(site_url('index.php/department/data'));
        }
        $data["title"] = 'Tambah Department';
        $this->render("department/add_form", $data);
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('index.php/department/data');
       
        $department = $this->DepartmentModel;
        $validation = $this->form_validation;
        $validation->set_rules($department->rules());

        if ($validation->run()) {
            $department->update();
            $this->session->set_flashdata('success', 'Data berhasil dirubah');
            redirect(site_url('index.php/department/data'));
        }

        $data["department"] = $department->getById($id);
        if (!$data["department"]) show_404();
        $data["title"] = 'Edit Department';
        
        $this->render("department/edit_form", $data);
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->DepartmentModel->delete($id)) {
            $this->session->set_flashdata('success', 'Data berhasil dihapus');
            redirect(site_url('index.php/department/data'));
        }
    }

    public function achievement()
    {
        $data["data"] = $this->DepartmentModel->getAll();
        $data["title"] = 'Achievement Department';
        $this->render("department/achievement", $data);
    }
}
