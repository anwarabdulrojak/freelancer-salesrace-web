<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Achievement extends MY_Controller {

    public function __construct(){
        parent::__construct();

        $this->load->model(['TargetStoreModel','AchievementStoreModel']);
    }

    public function store(){
        
        //get target store
        $where = array('month'=>date('m'), 'year'=>date('Y'));        
        $cek  = $this->TargetStoreModel->select($where);        
        $data['target'] = $cek;
        $data['month'] = date('F');
        $data['year'] = date('Y');
        $data['title'] = 'Set Achievement Store';

        $this->render('achievement/store/index', $data);

    }

    public function store_save(){
        $nilai = $this->input->post('nilai');
        $date = date('Y-m-d');
        $where = array('MONTH(date)'=> date('m'),'YEAR(date)'=> date('Y'));
        
        $cek  = $this->AchievementStoreModel->select($where);
        if(!empty($cek)){
            
            $array = array('nilai'=>$nilai);

            $sql = $this->AchievementStoreModel->update($where, $array);
        }else{                
            $array = array(
                'nilai'=>$nilai,
                'date'=>$date
            );

            $sql = $this->AchievementStoreModel->insert($array);
        }
        // Cek apakah query insert nya sukses atau gagal
        if($sql){ // Jika sukses
            echo "<script>alert('Data berhasil disimpan');window.location = '".base_url('index.php/achievement/store')."';</script>";
        }else{ // Jika gagal
            echo "<script>alert('Data gagal disimpan');window.location = '".base_url('index.php/achievement/store')."';</script>";        
        }
    }
}