<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spn_bonus extends MY_Controller {

  public function __construct(){
      parent::__construct();

      $this->load->model(['UserModel']);
  }

  public function index(){
    
      $data['title'] = 'SPN Bonus';
      $data['data'] = $this->UserModel->get_all_sales();
      $this->render('spn_bonus/index', $data);
  }

}