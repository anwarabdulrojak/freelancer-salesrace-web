<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Set_target extends MY_Controller {

    public function __construct(){
        parent::__construct();

        $this->load->model(['DepartmentModel','UserModel','TargetModel']);
    }

    public function store(){
        $data['month'] = date('F');
        $data['year'] = date('Y');
        $data['title'] = 'Set Target Store';

        $this->render('set_target/store/index', $data);

    }

    public function store_save(){
        $nilai = $this->input->post('nilai');
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        $where = array('month'=>$month, 'year'=>$year, 'type'=>1);
        
        $cek  = $this->TargetModel->select($where);
        if(!empty($cek)){
            
            $array = array('nilai'=>$nilai);

            $sql = $this->TargetModel->update($where, $array);
        }else{                
            $array = array(
                'nilai'=>$nilai,
                'month'=>$month,
				'year'=>$year,
				'type' => 1
            );

            $sql = $this->TargetModel->insert($array);
        }
        // Cek apakah query insert nya sukses atau gagal
        if($sql){ // Jika sukses
            echo "<script>alert('Data berhasil disimpan');window.location = '".base_url('index.php/set_target/store')."';</script>";
        }else{ // Jika gagal
            echo "<script>alert('Data gagal disimpan');window.location = '".base_url('index.php/set_target/store')."';</script>";        
        }
    }

    public function department(){
        $data['department'] = $this->DepartmentModel->get();
        $data['title'] = 'Set Target Department';
        $this->render('set_target/department/index', $data);
    }

    public function department_save(){
        $nilai = $this->input->post('nilai');
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        $department_id = $this->input->post('department_id');
        $where = array('month' => $month, 'year' => $year, 'type' => 2, 'department_id' => $department_id);
        
        $cek  = $this->TargetModel->select($where);
        if(!empty($cek)){
            
            $array = array('nilai' => $nilai);

            $sql = $this->TargetModel->update($where, $array);
        }else{                
            $array = array(
                'department_id' => $department_id,
                'nilai' => $nilai,
                'month' => $month,
				'year' => $year,
				'type' => 2
            );

            $sql = $this->TargetModel->insert($array);
        }
        // Cek apakah query insert nya sukses atau gagal
        if($sql){ // Jika sukses
            echo "<script>alert('Data berhasil disimpan');window.location = '".base_url('index.php/set_target/department')."';</script>";
        }else{ // Jika gagal
            echo "<script>alert('Data gagal disimpan');window.location = '".base_url('index.php/set_target/department')."';</script>";        
        }
    }

    public function sales_executive(){
        $data['sales'] = $this->UserModel->get_all_sales();
        $data['title'] = 'Set Target Sales Executive';
        $this->render('set_target/sales_executive/index', $data);
    }

    public function sales_save(){
        $nilai = $this->input->post('nilai');
        $month = $this->input->post('month');
        $year = $this->input->post('year');
        $sales_id = $this->input->post('sales_id');
        $where = array('month' => $month, 'year' => $year, 'type' => 3, 'sales_id' => $sales_id);
        
        $cek  = $this->TargetModel->select($where);
        if(!empty($cek)){
            
            $array = array('nilai' => $nilai);

            $sql = $this->TargetModel->update($where, $array);
        }else{                
            $array = array(
                'sales_id' => $sales_id,
                'nilai' => $nilai,
                'month' => $month,
				'year' => $year,
				'type' => 3
            );

            $sql = $this->TargetModel->insert($array);
        }
        // Cek apakah query insert nya sukses atau gagal
        if($sql){ // Jika sukses
            echo "<script>alert('Data berhasil disimpan');window.location = '".base_url('index.php/set_target/department')."';</script>";
        }else{ // Jika gagal
            echo "<script>alert('Data gagal disimpan');window.location = '".base_url('index.php/set_target/department')."';</script>";        
        }
	}
	
	public function department_import()
    {
        // Load plugin PHPExcel nya
        include APPPATH.'third_party/PHPExcel/PHPExcel.php';

        $config['upload_path'] = realpath('upload');
        $config['allowed_types'] = 'xlsx|xls|csv';
        $config['max_size'] = '10000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {

            //upload gagal
            $this->session->set_flashdata('notif', '<div class="alert alert-danger"><b>PROSES IMPORT GAGAL!</b> '.$this->upload->display_errors().'</div>');
            //redirect halaman
            redirect('index.php/set_target/department');

        } else {

            $data_upload = $this->upload->data();

            $excelreader = new PHPExcel_Reader_Excel2007();
            $loadexcel = $excelreader->load('upload/'.$data_upload['file_name']); // Load file yang telah diupload ke folder excel
            $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

            $data = array();

            $numrow = 1;
            foreach($sheet as $row){
				if($numrow > 1){
					$cekDept  = $this->DepartmentModel->select(array('kode' => $row['A'], 'nama' => $row['B']));
					if (!empty($cekDept)) {
						$where = array('month' => $row['C'], 'year' => $row['D'], 'type' => 2, 'department_id' => $cekDept->id);
						$cekTarget  = $this->TargetModel->select($where);
						if(!empty($cekTarget)){
							$array = array('nilai' => $row['E']);
							$sql = $this->TargetModel->update($where, $array);
						}else{
							$array = array(
								'department_id' => $cekDept->id,
								'nilai' => $row['E'],
								'month' => $row['C'],
								'year' => $row['D'],
								'type' => 2
							);
				
							$sql = $this->TargetModel->insert($array);
						}
					}
				}
                $numrow++;
            }
            //delete file from server
            unlink(realpath('upload/'.$data_upload['file_name']));

            //upload success
            $this->session->set_flashdata('notif', '<div class="alert alert-success"><b>PROSES IMPORT BERHASIL!</b> Data berhasil diimport!</div>');
            //redirect halaman
            redirect('index.php/set_target/department');

        }
    }
	
	public function sales_import()
    {
        // Load plugin PHPExcel nya
        include APPPATH.'third_party/PHPExcel/PHPExcel.php';

        $config['upload_path'] = realpath('upload');
        $config['allowed_types'] = 'xlsx|xls|csv';
        $config['max_size'] = '10000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {

            //upload gagal
            $this->session->set_flashdata('notif', '<div class="alert alert-danger"><b>PROSES IMPORT GAGAL!</b> '.$this->upload->display_errors().'</div>');
            //redirect halaman
            redirect('index.php/set_target/sales_executive');

        } else {

            $data_upload = $this->upload->data();

            $excelreader = new PHPExcel_Reader_Excel2007();
            $loadexcel = $excelreader->load('upload/'.$data_upload['file_name']); // Load file yang telah diupload ke folder excel
            $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

            $data = array();

            $numrow = 1;
            foreach($sheet as $row){
				if($numrow > 1){
                    $cekSales  = $this->UserModel->select(array('nip' => $row['A'], 'nama' => $row['B'], 'role' => 2));
					if (!empty($cekSales)) {
						$where = array('month' => $row['C'], 'year' => $row['D'], 'type' => 2, 'sales_id' => $cekSales->id);
						$cekTarget  = $this->TargetModel->select($where);
						if(!empty($cekTarget)){
							$array = array('nilai' => $row['E']);
							$sql = $this->TargetModel->update($where, $array);
						}else{
							$array = array(
								'sales_id' => $cekSales->id,
								'nilai' => $row['E'],
								'month' => $row['C'],
								'year' => $row['D'],
								'type' => 3
							);
				
							$sql = $this->TargetModel->insert($array);
						}
					}
				}
                $numrow++;
            }
            //delete file from server
            unlink(realpath('upload/'.$data_upload['file_name']));

            //upload success
            $this->session->set_flashdata('notif', '<div class="alert alert-success"><b>PROSES IMPORT BERHASIL!</b> Data berhasil diimport!</div>');
            //redirect halaman
            redirect('index.php/set_target/sales_executive');

        }
    }
}
