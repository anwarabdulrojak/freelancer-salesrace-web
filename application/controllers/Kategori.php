<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("KategoriModel");
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data["data"] = $this->KategoriModel->getAll();
        $data["title"] = 'Kategori';
        $this->render("manajemen/kategori/index", $data);
    }

    public function add()
    {
        $kategori = $this->KategoriModel;
        $validation = $this->form_validation;
        $validation->set_rules($kategori->rules());

        if ($validation->run()) {
            $kategori->save();
            $this->session->set_flashdata('success', 'Data berhasil disimpan');
            redirect(site_url('index.php/manajemen/kategori'));
        }

        $data["title"] = 'Tambah Kategori';
        $this->render("manajemen/kategori/add_form", $data);
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('index.php/manajemen/kategori');
       
        $kategori = $this->KategoriModel;
        $validation = $this->form_validation;
        $validation->set_rules($kategori->rules());

        if ($validation->run()) {
            $kategori->update();
            $this->session->set_flashdata('success', 'Data berhasil dirubah');
            redirect(site_url('index.php/manajemen/kategori'));
        }

        $data["kategori"] = $kategori->getById($id);
        if (!$data["kategori"]) show_404();
        $data["title"] = 'Edit Kategori';
        
        $this->render("manajemen/kategori/edit_form", $data);
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->KategoriModel->delete($id)) {
            $this->session->set_flashdata('success', 'Data berhasil dihapus');
            redirect(site_url('index.php/manajemen/kategori'));
        }
    }
}
