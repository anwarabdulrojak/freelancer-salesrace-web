<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sbc extends MY_Controller {

    public function __construct(){
        parent::__construct();

        $this->load->model(['AchievementModel']);
        $this->load->library('form_validation');
    }

    public function index(){
		
		$date = date("Y-m-d");
        $data["data"] = $this->AchievementModel->getSbc($this->session->userdata('user_id'));
		$data['title'] = 'Sales Book Control';
		
        $this->render('sbc/index', $data);

	}
	
	public function add()
    {
		$data["title"] = 'Tambah achievement';
		
        $this->render("sbc/add_form", $data);
    }

    public function save(){
        $nilai = $this->input->post('nilai');
        $date = date("Y-m-d", strtotime($this->input->post('date')));
        $user_id = $this->input->post('user_id');

        $where = array('date'=>$date,'user_id'=>$user_id);
        $cek  = $this->AchievementModel->select($where);
        if(!empty($cek)){
            
            $array = array('nilai'=>$nilai);

            $sql = $this->AchievementModel->update($where, $array);
        }else{
            $array = array(
                'nilai'=>$nilai,
                'date'=>$date,
                'user_id'=>$user_id
            );

            $sql = $this->AchievementModel->insert($array);
        }
        // Cek apakah query insert nya sukses atau gagal
        if($sql){ // Jika sukses
            echo "<script>alert('Data berhasil disimpan');window.location = '".base_url('index.php/sbc')."';</script>";
        }else{ // Jika gagal
            echo "<script>alert('Data gagal disimpan');window.location = '".base_url('index.php/sbc/add')."';</script>";
        }
    }
}
