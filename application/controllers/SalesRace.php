<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salesrace extends MY_Controller {

    public function __construct(){
        parent::__construct();

        $this->load->model(['UserModel']);
    }

    public function index(){
        
        $data['week']  = $this->UserModel->getSalesRaceWeek();
        $data['month']  = $this->UserModel->getSalesRaceMonth();
        $data['title'] = 'Sales Race';
        $this->render('salesrace/index', $data);

    }
}