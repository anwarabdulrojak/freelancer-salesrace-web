<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("DepartmentModel");
    }

  	public function home(){    
		$data = array();
		$data['title'] = 'Dashboard';
        $data["department"] = $this->DepartmentModel->getAll();
		
		$this->render('home', $data);
  	}

}
