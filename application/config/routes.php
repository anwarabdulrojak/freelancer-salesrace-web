<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = "auth";
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// $route['set_target/kategori/(:num)'] = "set_target/kategori_edit/$1";

$route['set_target/store/save'] = "set_target/store_save";
$route['set_target/department/save'] = "set_target/department_save";
$route['set_target/department/import'] = "set_target/department_import";
$route['set_target/sales_executive/save'] = "set_target/sales_save";
$route['set_target/sales_executive/import'] = "set_target/sales_import";

$route['department/add'] = "department/add";
$route['department/edit/(:num)'] = "department/edit/$1";
$route['department/delete/(:num)'] = "department/delete/$1";

$route['sales_executive/add'] = "sales_executive/add";
$route['sales_executive/edit/(:num)'] = "sales_executive/edit/$1";
$route['sales_executive/delete/(:num)'] = "sales_executive/delete/$1";

// $route['achievement/store/save'] = "achievement/store_save";

// $route['manajemen/kategori'] = "kategori";
// $route['manajemen/kategori/add'] = "kategori/add";
// $route['manajemen/kategori/edit/(:num)'] = "kategori/edit/$1";
// $route['manajemen/kategori/delete/(:num)'] = "kategori/delete/$1";

// $route['manajemen/department'] = "department";
// $route['manajemen/department/add'] = "department/add";
// $route['manajemen/department/edit/(:num)'] = "department/edit/$1";
// $route['manajemen/department/delete/(:num)'] = "department/delete/$1";

// $route['manajemen/user'] = "user";
// $route['manajemen/user/add'] = "user/add";
// $route['manajemen/user/edit/(:num)'] = "user/edit/$1";
// $route['manajemen/user/delete/(:num)'] = "user/delete/$1";
